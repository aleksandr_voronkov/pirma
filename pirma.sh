for image in $(find "/Users/aleksandrvoronkov/pirma_uzduotis" -name \*."tar.gz"); do
    printf "\e[33m#########################docker load#########################\e[0m\n"
    variable=$(docker load -i $image | cut -d':' -f 2)
    variable=$(echo $variable|tr -d '\n')
    printf "\e[32m#########################docker tag#########################\e[0m\n"
    docker tag $variable localhost:5000/$variable
    printf "\e[31m#########################docker push#########################\e[0m\n"
    docker push localhost:5000/$variable
    printf "\e[34m#########################docker image rm#########################\e[0m\n"
    docker image rm localhost:5000/$variable
  done